# CERTH-ITI-VCL3D
# Authors: {petros.drakoulis, skaravarsamis}@iti.gr

# INHERIT FROM BASE IMAGE
FROM nvcr.io/nvidia/cuda:11.6.0-cudnn8-devel-ubuntu20.04

# INSTALL PYTHON AND PIP
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get upgrade -y
RUN apt install -y software-properties-common
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt-get install -y apt-utils
RUN apt-get install -y python3.10
RUN apt-get install -y python3.10-dev
RUN update-alternatives --install /usr/bin/python3 python /usr/bin/python3.10 1
RUN apt install curl -y
RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10

# INSTALL THE REST OF THE PACKAGES
RUN apt install -y unzip
RUN apt install -y net-tools
RUN apt install -y git
RUN apt install -y libopencv-dev
RUN apt install -y iputils-ping
RUN apt install -y nano

RUN pip install --upgrade pip
RUN pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
RUN pip3 install sqlalchemy==2.0.27
RUN pip3 install pyramid_mailer==0.15.1
RUN pip3 install packaging==23.2
RUN pip3 install deepspeed==0.14.4
RUN pip3 install fairscale==0.4.13
RUN pip3 install ftfy==6.1.3
RUN pip3 install regex==2023.12.25
RUN pip3 install tqdm==4.66.2
RUN pip3 install scikit-image==0.22.0
RUN pip3 install matplotlib==3.8.3
RUN pip3 install timm==0.9.16
RUN pip3 install imutils==0.5.4
RUN pip3 install scipy==1.11.4
RUN pip3 install pandas==2.2.0
RUN pip3 install h5py==3.10.0
RUN pip3 install av==11.0.0
RUN pip3 install einops==0.7.0
RUN pip3 install addict==2.4.0
RUN pip3 install yapf==0.40.2
RUN pip3 install boto3==1.34.46
RUN pip3 install easydict==1.12
RUN pip3 install tensorboardx==2.6.2.2
RUN pip3 install opencv-python==4.9.0.80
RUN pip3 install fastapi==0.93.0
RUN pip3 install numpy==1.24.2
RUN pip3 install uvicorn==0.20.0
RUN pip3 install pydantic==1.10.6
RUN pip3 install requests==2.22.0
RUN pip3 install wget==3.2
RUN pip3 install python-multipart==0.0.6
RUN pip3 install websockets==12.0

WORKDIR /tmp/
RUN git clone https://github.com/NVIDIA/apex
RUN cd /tmp/apex/ && \
    python3.10 setup.py install

# MAKE CONTAINER FILESYSTEM
ADD code /code

WORKDIR /
RUN mkdir action
ADD exec /action

# PORT-FORWARDING
EXPOSE 5042

# SET EXECUTION STARTING POINT
RUN chmod +rx /action/exec

ENTRYPOINT ["bash", "/action/exec"]
