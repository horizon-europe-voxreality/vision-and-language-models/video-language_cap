import numpy as np
import math
import json
import sys
import os

if __name__ == '__main__':

    MERGE_DATASETS = False
    TRAINING_DATA_PORTION = 0.8

    ### READ data from the original MSRVTT dataset ###

    fp_label = open('train.label.tsv','r')
    fp_caption = open('train.caption.tsv','r')
    fp_caption_linelist = open('train.caption.linelist.tsv','r')

    img_list = []
    label_list = []
    caption_list = []
    caption_linelist_list = []

    for line in fp_caption.readlines():
        line = line.replace('\n','')
        parts = line.split('\t')

        videofile = parts[0]
        captions = eval(parts[1])

        img_list.append(os.path.basename(videofile.replace('.mp4','')))
        label_list.append(list(np.unique(np.array(captions).astype(str))))
        caption_list.append(list(np.unique(np.array(captions).astype(str))))
        caption_linelist_list.append(list(np.unique(np.array(captions).astype(str))))

    fp_label.close()
    fp_caption.close()
    fp_caption_linelist.close()

    ### Create the reduced dataset  ###

    fp_caption = open('SPATIAL_DATASET','r')

    sp_img_list = []
    sp_label_list = []
    sp_caption_list = []
    sp_caption_linelist_list = []

    idx = 0
    for line in fp_caption.readlines():
        line = line.replace('\n','')
        
        idx += 1
        
        parts = line.split('\t')

        videofile = parts[0]
        captions = eval(parts[1])

        sp_img_list.append(os.path.basename(videofile.replace('.mp4','')))
        sp_label_list.append(list(np.unique(np.array(captions).astype(str))))
        sp_caption_list.append(list(np.unique(np.array(captions).astype(str))))
        sp_caption_linelist_list.append(list(np.unique(np.array(captions).astype(str))))

    ### Merge the two datasets ###

    if MERGE_DATASETS == False:
        ### Make the training data portion ###
        sp_fp_img = open('SPATIAL.train.img.tsv','w+')
        sp_fp_img_lineidx = open('SPATIAL.train.img.lineidx','w+')
        sp_fp_label = open('SPATIAL.train.label.tsv','w+')
        sp_fp_label_lineidx = open('SPATIAL.train.label.lineidx','w+')
        sp_fp_linelist = open('SPATIAL.train.linelist.tsv','w+')
        sp_fp_linelist_lineidx = open('SPATIAL.train.linelist.lineidx','w+')
        sp_fp_caption = open('SPATIAL.train.caption.tsv','w+')
        sp_fp_caption_lineidx = open('SPATIAL.train.caption.lineidx','w+')
        sp_fp_caption_linelist = open('SPATIAL.train.caption.linelist.tsv','w+')

        for idx_ in range(0, math.floor(TRAINING_DATA_PORTION*len(sp_img_list)),1):
             sp_fp_img.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\tdatasets/spatial/videos/{sp_img_list[idx_]}.mp4\n")
             sp_fp_img_lineidx.write(f"{idx_}\n")
             
             sp_fp_label_lineidx.write(f"{idx_}\n")
             sp_fp_label.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]}\n")
             sp_fp_caption.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]}\n")
             sp_fp_caption_lineidx.write(f"{idx_}\n") 
             sp_fp_linelist.write(f"{idx_}\n")
             sp_fp_linelist_lineidx.write(f"{idx_}\n")
 
             for capidx_ in range(len(sp_label_list[idx_])):
                  sp_fp_caption_linelist.write(f"{idx_}\t{capidx_}\n")
    
        sp_fp_img.close()
        sp_fp_img_lineidx.close()
        sp_fp_label.close()
        sp_fp_label_lineidx.close()
        sp_fp_linelist.close()
        sp_fp_linelist_lineidx.close()
        sp_fp_caption.close()
        sp_fp_caption_lineidx.close()
        sp_fp_caption_linelist.close()

        ### Make the testing data portion ###

        sp_fp_img = open('SPATIAL.test.img.tsv','w+')
        sp_fp_img_lineidx = open('SPATIAL.test.img.lineidx','w+')
        sp_fp_linelist = open('SPATIAL.test.linelist.tsv','w+')
        sp_fp_linelist_lineidx = open('SPATIAL.test.linelist.lineidx','w+')
        sp_fp_label = open('SPATIAL.test.label.tsv','w+')
        sp_fp_label_lineidx = open('SPATIAL.test.label.lineidx','w+')
        sp_fp_caption_lineidx = open('SPATIAL.test.caption.lineidx','w+')
        sp_fp_caption_linelist = open('SPATIAL.test.caption.linelist.tsv','w+')
        sp_fp_caption = open('SPATIAL.test.caption.tsv','w+')
        sp_fp_caption_coco = open('SPATIAL.test.caption_coco_format.json','w+')

        coco_captions = {'annotations': [], 'images': [], 'type': 'captions', 'info' : 'dummy', 'licenses' : 'dummy'}

        id_idx_ = 0

        for idx_ in range(math.ceil(TRAINING_DATA_PORTION*len(sp_img_list)),len(sp_img_list),1):
             sp_fp_img.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\tdatasets/spatial/videos/{sp_img_list[idx_]}.mp4\n")
             sp_fp_label.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]}\n")
             sp_fp_caption.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]}\n")
             sp_fp_caption_lineidx.write(f"{idx_}\n")
             sp_fp_label_lineidx.write(f"{idx_}\n")
             sp_fp_img_lineidx.write(f"{idx_}\n")
             sp_fp_linelist.write(f"{idx_}\n")
             sp_fp_linelist_lineidx.write(f"{idx_}\n")
             
             for capidx_ in range(len(sp_label_list[idx_])):
                 sp_fp_caption_linelist.write(f"{idx_}\t{capidx_}\n")
               
                 coco_captions['annotations'].append({"image_id":f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4","caption":eval(sp_label_list[idx_][capidx_]),"id":id_idx_})
                 id_idx_ += 1

        json.dump(coco_captions, sp_fp_caption_coco)
 
        sp_fp_img.close()
        sp_fp_img_lineidx.close()
        sp_fp_linelist.close()
        sp_fp_linelist_lineidx.close()
        sp_fp_label.close()
        sp_fp_label_lineidx.close()
        sp_fp_caption.close()
        sp_fp_caption_linelist.close()
        sp_fp_caption_lineidx.close()
        sp_fp_caption_coco.close()

    else:
        ### Make the training data portion ###
        sp_fp_img = open('SPATIAL.train.img.tsv','w+')
        sp_fp_img_lineidx = open('SPATIAL.train.img.lineidx','w+')
        sp_fp_label = open('SPATIAL.train.label.tsv','w+')
        sp_fp_label_lineidx = open('SPATIAL.train.label.lineidx','w+')
        sp_fp_caption = open('SPATIAL.train.caption.tsv','w+')
        sp_fp_caption_lineidx = open('SPATIAL.train.caption.lineidx','w+')
        sp_fp_caption_linelist = open('SPATIAL.train.caption.linelist.tsv','w+')
        sp_fp_linelist = open('SPATIAL.train.linelist.tsv','w+')
        sp_fp_linelist_lineidx = open('SPATIAL.train.linelist.lineidx','w+')

        line_idx_ = 0
        for idx_ in range(0, math.floor(TRAINING_DATA_PORTION*len(sp_img_list)),1):
             try:
                 idx_orig_ = img_list.index(sp_img_list[idx_])
             except:
                 idx_orig_ = -1
             
             if idx_orig_ == -1:
                 continue

             sp_fp_img.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\tdatasets/spatial/videos/{sp_img_list[idx_]}.mp4\n")
             sp_fp_label.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]+label_list[idx_orig_]}\n")
             sp_fp_caption.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]+label_list[idx_orig_]}\n")
             sp_fp_linelist.write(f"{idx_}\n")

             for capidx_ in range(len(sp_label_list[idx_])):
                 sp_fp_linelist_lineidx.write(f"{line_idx_}\n")
                 sp_fp_img_lineidx.write(f"{line_idx_}\n")
                 sp_fp_label_lineidx.write(f"{line_idx_}")
                 sp_fp_caption_linelist.write(f"{idx_}\t{capidx_}\n")
                 sp_fp_caption_lineidx.write(f"{idx_}\n")
                 line_idx_ += 1

        sp_fp_img.close()
        sp_fp_img_lineidx.close()
        sp_fp_label.close()
        sp_fp_label_lineidx.close()
        sp_fp_caption.close()
        sp_fp_caption_lineidx.close()
        sp_fp_caption_linelist.close()
        sp_fp_linelist.close()
        sp_fp_linelist_lineidx.close()

        ### Make the testing data portion ###

        sp_fp_img = open('SPATIAL.test.img.tsv','w+')
        sp_fp_img_lineidx = open('SPATIAL.test.img.lineidx','w+')
        sp_fp_linelist = open('SPATIAL.test.linelist.tsv','w+')
        sp_fp_linelist_lineidx = open('SPATIAL.test.linelist.lineidx','w+')
        sp_fp_label = open('SPATIAL.test.label.tsv','w+')
        sp_fp_label_lineidx = open('SPATIAL.test.label.lineidx','w+')
        sp_fp_caption = open('SPATIAL.test.caption.tsv','w+')
        sp_fp_caption_lineidx = open('SPATIAL.test.caption.lineidx','w+')
        sp_fp_caption_linelist = open('SPATIAL.test.caption.linelist.tsv','w+')

        coco_captions = {'annotations': [], 'images': [], 'type': 'captions', 'info' : 'dummy', 'licenses' : 'dummy'}
        
        id_idx_ = 0
        line_idx_ = 0

        for idx_ in range(math.ceil(TRAINING_DATA_PORTION*len(sp_img_list)),len(sp_img_list),1):
             try:
                 idx_orig_ = img_list.index(sp_img_list[idx_])
             except:
                 idx_orig_ = -1

             if idx_orig_ == -1:
                 continue

             sp_fp_img.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\tdatasets/spatial/videos/{sp_img_list[idx_]}.mp4\n")
             sp_fp_img_lineidx.write(f"{idx_}\n")
             sp_fp_linelist.write(f"{idx_}\n")
             sp_fp_label.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]+label_list[idx_orig_]}\n")
             sp_fp_caption.write(f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4\t{sp_label_list[idx_]+label_list[idx_orig_]}\n")

             coco_captions['images'].append({"id":f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4","file_name":f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4"})

             sp_label_list_unique_ = list(set(sp_label_list[idx_]))
             for capidx_ in range(len(sp_label_list_unique_)):
                 coco_captions['annotations'].append({"image_id":f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4","caption":eval(sp_label_list_unique_[capidx_])["caption"],"id":id_idx_})
                 
                 sp_fp_caption_lineidx.write(f"{idx_orig_}\n")
                 sp_fp_caption_linelist.write(f"{idx_}\t{capidx_}\n")
                 sp_fp_linelist_lineidx.write(f"{idx_orig_}\n")
                 sp_fp_label_lineidx.write(f"{idx_orig_}\n")
                 
                 id_idx_ += 1
                 line_idx_ += 1

             label_list_unique_ = list(set(label_list[idx_orig_]))
             for capidx_ in range(len(label_list_unique_)):
                 coco_captions['annotations'].append({"image_id":f"datasets/spatial/videos/{sp_img_list[idx_]}.mp4","caption":eval(label_list_unique_[capidx_])["caption"],"id":id_idx_})
 
                 sp_fp_caption_lineidx.write(f"{idx_orig_}\n")
                 sp_fp_caption_linelist.write(f"{idx_}\t{capidx_}\n")
                 sp_fp_linelist_lineidx.write(f"{idx_orig_}\n")
                 sp_fp_label_lineidx.write(f"{idx_orig_}\n")
 
                 id_idx_ += 1
                 line_idx_ += 1

        with open('SPATIAL.test.caption_coco_format.json','w+') as sp_fp_caption_coco: 
             json.dump(coco_captions, sp_fp_caption_coco)
             sp_fp_caption_coco.close()

        sp_fp_img.close()
        sp_fp_img_lineidx.close()
        sp_fp_linelist.close()
        sp_fp_linelist_lineidx.close()
        sp_fp_label.close()
        sp_fp_label_lineidx.close()
        sp_fp_caption.close()
        sp_fp_caption_linelist.close()
        sp_fp_caption_lineidx.close()
        sp_fp_catpion_coco.close()
