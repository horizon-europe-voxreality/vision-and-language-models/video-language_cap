import time
import sys
import os
from typing import Union
from fastapi import FastAPI, Response, UploadFile, Request, Form
from fastapi.openapi.utils import get_openapi
from fastapi.responses import FileResponse, HTMLResponse
from pydantic import BaseModel
import json
import numpy as np
import tempfile
import shutil
import uvicorn
from datetime import datetime
import csv
import torch
from PIL import Image
import re
import video_caption
from fastapi.middleware.cors import CORSMiddleware
from src.tasks.run_caption_VidSwinBert_inference import CustomArgs
from src.tasks.run_caption_VidSwinBert_inference import main
from src.tasks.run_caption_VidSwinBert_inference import make_swinbert_model
from src.tasks.run_caption_VidSwinBert_inference import make_swinbert_caption

args = CustomArgs()

args.resume_checkpoint = 'models/custom-model/checkpoint-last/model.bin'
args.eval_model_dir = 'models/custom-model/checkpoint-last/'
args.test_video_fname = None
args.do_lower_case = True
args.do_test = True
args.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
args.add_od_labels = False
args.val_yaml = "datasets/spatial/test_32frames.yaml"

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["POST", "GET"],
    allow_headers=["*"],
)

args, vl_transformer, tokenizer = make_swinbert_model(args)

device = args.device

class Questions(BaseModel):
    file: UploadFile
    questions: str


@app.get("/")
async def read_root() -> dict:
    return {"message": "Welcome to Certh's API testing! Append to /docs to start using the api!"}

@app.post("/video-language_cap/")
async def create_upload_file(file:UploadFile):

    # create a new folder with the current date and time
    now = datetime.now()
    date_time = now.strftime("%Y%m%d-%H%M%S")
    folder_name = now.strftime("%Y-%m-%d")
    folder_path = os.path.join("Caption", folder_name)
    os.makedirs(folder_path, exist_ok=True)
    
    # Split the filename and extension
    filename, ext = os.path.splitext(file.filename)
    new_filename = f"{filename}-{date_time}{ext}"
    
    # save the uploaded file to the new folder
    file_path = os.path.join(folder_path, new_filename)
    with open(file_path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    args.test_video_fname = file_path

    tim_s = time.time()
    caption = video_caption.video_cap(args, vl_transformer, tokenizer)
    tim_e = time.time()
    total_t = tim_e-tim_s

    # write the questions and answers to a csv file
    csv_path = os.path.join(folder_path, f"{filename}-{date_time}.csv")

    with open(csv_path, mode='w', newline='') as csvfile:
        fieldnames = ['Video', 'Caption', 'Inference_time_sec']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'Video': new_filename, 'Caption': caption, 'Inference_time_sec' : total_t})
    
    # os.remove(file_path)  
    return ({"Video caption": caption, "Inference_time_sec" : total_t})

if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=5042)
