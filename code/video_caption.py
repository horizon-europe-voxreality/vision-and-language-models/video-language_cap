# from transformers import pipeline
import time
import main
import sys

from src.tasks.run_caption_VidSwinBert_inference import CustomArgs
from src.tasks.run_caption_VidSwinBert_inference import main
from src.tasks.run_caption_VidSwinBert_inference import make_swinbert_model
from src.tasks.run_caption_VidSwinBert_inference import make_swinbert_caption

def video_cap(args, vl_transformer, tokenizer):
    caption = make_swinbert_caption(args, vl_transformer, tokenizer)

    return caption


if __name__ == '__main__':
    args = CustomArgs()
    
    args.resume_checkpoint = 'models/custom-model/best-checkpoint/model.bin'
    args.eval_model_dir = 'models/custom-model/best-checkpoint/'
    args.test_video_fname = str(sys.argv[1])
    args.do_lower_case = True
    args.do_test = True
    args.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    args.add_od_labels = False
    args.val_yaml = "datasets/spatial/test_32frames.yaml"

    args, vl_transformer, tokenizer = make_swinbert_model(args)

    print(video_cap(args.test_video_fname, args, vl_transformer, tokenizer))
