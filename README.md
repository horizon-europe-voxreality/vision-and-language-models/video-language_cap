# Welcome to the VOXReality Horizon Europe project 

Below, you will find the necessary instructions in order to build and run the VOXReality Video Spatial Captioning service that is based on the Microsoft SwinBERT model architecture.

## 1. Requirements

In order to build the VOXReality Video Spatial Captioning docker image, you will need the following:

1. A CUDA-compatible GPU.
  1. We recommend at least 4 GB of GPU memory.
  2. The code was tested on the NVIDIA 535 proprietary driver.

2. On Linux, the software was tested on a computer running Ubuntu 20.04.6 LTS installed.
  1. Make sure docker is installed on your system.
  2. Make sure you have the NVIDIA Container Toolking installed. More info and instructions can be found in the [official installation guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)

3. On Windows-based systems, we tested the model on Windows 10 and Windows 11.
  1. Make sure docker is installed on your system.

One you have docker up and running, you can move to cloning the repository.

## 2. Cloning the repository

1. Start by cloning the repository by running the command `https://gitlab.com/horizon-europe-voxreality/vision-and-language-models/video-language_cap.git`
   
## 3. Building the docker image

In order to build the docker image from scratch, you should follow the step below:

1. Change into the folder where the Dockerfile resides and execute `sudo ./LINUX_Build_Image.sh`

## 4. Running the docker image

1. You can start the docker container by running the `sudo ./LINUX_Start_Contrainer.sh`.
2. Once the container has started, you can move to the `https://localhost:5042/docs` or `https://YourExternalIP:5042/docs` address in your web browser and start using the API.
3. To stop the container, run the `sudo ./LINUX_Stop_Container.sh` script.
